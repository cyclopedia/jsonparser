﻿using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using Newtonsoft.Json;

namespace JsonParser.Controllers
{
	public class HomeController : Controller
	{
		// GET: Home
		public ActionResult Index()
		{
			return View();
		}

		public string GetPreDefinedJson()
		{
			string json = null;
			try
			{
				using (StreamReader objStreamReader = new StreamReader(Server.MapPath("/Resources/resume.json")))
				{
					json = objStreamReader.ReadToEnd();
				}
			}
			catch (Exception)
			{
				throw;
			}

			return json;
		}
	}
}