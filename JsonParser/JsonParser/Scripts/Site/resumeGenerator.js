﻿/// <reference path="../angular.js" />
var myApp = angular.module("resumeGenerator", []).controller("resumeConverter", function ($scope, $http) {
	$scope.getResume = function () {
		// Get resume in json from server
		$http.get("/Home/GetPreDefinedJson").then(function (response) {
			$scope.resume = response.data;
			$scope.resumePage = "resume.html";
		});
	}
});